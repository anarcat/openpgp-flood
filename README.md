This repository contains the top ten certificates from the SKS pool,
when ordered by the number of signatures in the certificate.

The data was processed by [Marcus Brinkmann](https://twitter.com/lambdafu/status/1147216421442732032) but I fetched the keys
from the keyservers myself.

Here are the keys:

 * Yegor Timoshenko (SKS Exploit, 174612 sigs): `EC18 257D B217 46FC 7110  54BE B19C 61D6 1333 360C`
 * Robert J. Hansen (GnuPG, 149113 sigs): `CC11 BE7C BBED 77B1 20F3 7B01 1DCB DC01 B444 27C7`
 * Phil Zimmermann (PGP author, 101023 sigs): `055F C78F 1121 9349 2C4F  37AF C746 3639 B2D7 795E`
 * [Tor Browser Developers (Tor, 100245 sigs)](https://openpgpkey.torproject.org/.well-known/openpgpkey/torproject.org/hu/kounek7zrdx745qydx6p59t9mqjpuhdf): `EF6E 286D DA85 EA2A 4BA7  DE68 4E2C 6E87 9329 8290`
 * Patrick Brunschwig (Enigmail, 100145 sigs): `4F9F 89F5 505A C1D1 A260 631C DB11 87B9 DD5F 693B`
 * Ryan McGinnis (GnuPG-Users 100001 sigs): `5C73 8727 EE58 786A 777C 4F1D B5AA 3FA3 486E D7AD`
 * Micah Lee (Intercept, 84650 sigs): `927F 419D 7EC8 2C2F 149C  1BD1 403C 2657 CD99 4F73`
 * [Daniel Kahn Gillmor](https://openpgpkey.debian.org/.well-known/openpgpkey/debian.org/hu/sr4so3py756t9p5ktpud9menxx1m3g5b) (Debian, 54616 sigs): `C4BC 2DDB 38CC E964 85EB  E9C2 F206 9117 9038 E5C6`
 * Patrick Brunschwig (Enigmail, 51343 sigs): `6D67 E781 7D58 8BEA 263F 41B9 EE81 92A6 E443 D6D8`
 * Lance Cottrell (Mixmaster, 34390 sigs): `33D5 1B56 2195 3173 AB74 B521 BDCA 9F8E 3A6C 1785`

Do not import those keys in your GnuPG keyring, as they might make
your GnuPG configuration unusable. Those are provided for testing
purposes only.

See also [my blog post on the topic](https://anarc.at/blog/2019-07-30-pgp-flooding-attacks/).
